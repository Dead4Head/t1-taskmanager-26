package ru.t1.amsmirnov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    public static final String DESCRIPTION = "Shows user profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER PROFILE]");
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

}
