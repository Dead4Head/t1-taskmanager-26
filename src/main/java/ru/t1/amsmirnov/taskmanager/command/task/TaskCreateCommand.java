package ru.t1.amsmirnov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.SCANNER.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @NotNull final String taskDescription = TerminalUtil.SCANNER.nextLine();
        @Nullable final String userId = getUserId();
        getTaskService().create(userId, taskName, taskDescription);
    }

}
