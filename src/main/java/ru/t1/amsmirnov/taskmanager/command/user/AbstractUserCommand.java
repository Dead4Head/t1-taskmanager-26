package ru.t1.amsmirnov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.IUserService;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@NotNull final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
