package ru.t1.amsmirnov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update current user's profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter first name: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUserById(
                userId, firstName, lastName, middleName
        );
    }

}
