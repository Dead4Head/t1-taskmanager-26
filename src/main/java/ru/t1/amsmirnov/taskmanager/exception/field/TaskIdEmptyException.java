package ru.t1.amsmirnov.taskmanager.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}
